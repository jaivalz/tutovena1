import axios from "axios";

const PersonService = {};

PersonService.getPersons = async () => {
  try {
    const response = await axios.get("people");


    console.log(response.data);

    const persons = [];

    response.data.results.forEach(person => {
      const data = {
        name: person.name,
        height: person.height
        };
      persons.push(data);
    });

    // const person = response.data.results;

    return persons;
  } catch (error) {
    const errorStatus = error.response.status;
    let errorMessage = "";

    if (errorStatus === 404) {
      errorMessage = "No se encontró al personaje";
    } else {
      errorMessage = "Ocurrió un error";
    }

    throw new Error(errorMessage);
  }
};

export default PersonService;
