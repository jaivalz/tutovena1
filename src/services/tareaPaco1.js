export default {
  data() {
    return {
      loading: false,
      logs: null,
      users: null,
      device: null,
      devices: null,
      errorMessage: ""
    };
  },
  methods: {
    getDataLog() {
      try {
        // Init loading
        this.loading = true;

        // Initialized values
        this.logs = null;

        // Get logs
        const idDevice = this.$router.param.idDevice;

        this.logs = getLogs(idDevice);

      } catch (error) {
        const errorStatus = error.response.status;
        let errorMessage = "";

        if (errorStatus === 404) {
          errorMessage = "blahblah";
        } else {
          errorMessage = "blehbleh";
        }

        throw new Error(errorMessage);
      } finally {
        this.loading = false;
      }
    },
    getDataUsers() {
      try {
        // Init loading
        this.loading = true;

        // Initialized values
        this.users = null;

        // Get users
        const idDevice = this.$router.param.idDevice;

        this.users = getUsers(idDevice);

      } catch (error) {
        const errorStatus = error.response.status;
        let errorMessage = "";

        if (errorStatus === 404) {
          errorMessage = "blahblah";
        } else {
          errorMessage = "blehbleh";
        }

        throw new Error(errorMessage);
      } finally {
        this.loading = false;
      }
    },
    getDataDevice() {
      try {
        // Init Loading
        this.loading = true;

        // Initialized values
        this.device = null;
        this.devices = null;

        // Get devices
        const idDevice = this.$router.param.idDevice;

        this.device = await getDevice(idDevice);
        this.devices = getDevices(device.installationID);

      } catch (error) {
        const errorStatus = error.response.status;
        let errorMessage = "";

        if (errorStatus === 404) {
          errorMessage = "blahblah";
        } else {
          errorMessage = "blehbleh";
        }

        throw new Error(errorMessage);
      } finally {
        this.loading = false;
      }
    },

    created() {
      // Get deviceId from route param
      const idDevice = this.$router.param.idDevice;

      this.getData(idDevice);
    }
  }
}
