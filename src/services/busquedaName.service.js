import axios from "axios";

const BusquedaNameService = {};

BusquedaNameService.getName = async name => {
  try {
    const result = await axios.get(`people/?search=${name}`);

    console.log(result.data.results); // Este console me muestra los objetos obtenidos al hacer la peticion

    const people = result.data.results; // Resultado del objeto
    return people;
  } catch (error) {
    const errorStatus = error.response.status;
    let errorMessage = "";

    if (errorStatus === 404) {
      errorMessage = "No se encontró al personaje";
    } else {
      errorMessage = "Ocurrió un error";
    }

    throw new Error(errorMessage);
  }
};

export default BusquedaNameService;
