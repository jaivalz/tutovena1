import axios from "axios";

const PeopleService = {};

// PeoplesService.getPeoples = async () => {}; - Para sacar el listado de planetas con un v-for

PeopleService.getPeople = async id => {
  try {
    const result = await axios.get(`people/${id}`);

    // Servicio para sacar la especie

    const specieRoute = result.data.species[0].split("/");
    const idSpecie = specieRoute[specieRoute.length - 2];
    const especie = await axios.get(`species/${idSpecie}`);

    // Servicio para sacar el planeta

    const planetRoute = result.data.homeworld.split("/");
    const idPlanet = planetRoute[planetRoute.length - 2];

    const planeta = await axios.get(`planets/${idPlanet}`);

    console.log(result.data);
    console.log(idSpecie);
    console.log(especie.data);
    console.log(planeta.data);

    const people = {
      nombre: result.data.name,
      altura: result.data.height,
      peso: result.data.mass,
      genero: result.data.gender,
      piel: result.data.skin_color,
      especie: especie.data.name, // Lo saco del servicio de especies
      planeta: planeta.data.name // Lo saco del servicio de planetas
    };
    console.log('respuesta', people)
    return people;
  } catch (error) {
    const errorStatus = error.response.status;
    let errorMessage = "";

    if (errorStatus === 404) {
      errorMessage = "No se encontró al personaje";
    } else {
      errorMessage = "Ocurrió un error";
    }

    throw new Error(errorMessage);
  }
};

export default PeopleService;
